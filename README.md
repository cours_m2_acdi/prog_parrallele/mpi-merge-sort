# Tri fusion parallèle avec MPI.

Fait dans le cadre du cours de Programmation Parallèle du master ACDI à l'Université d'Angers

http://info.univ-angers.fr/pub/richer/ensm2_para_crs5.php

Deux programmes :
* `MPI_sort_snd_rcv` utilisant la communication via `send` & `receive`
    * Arguments : `-s XXX` avec XXX la taille du tableau
* `MPI_sort` utilisant les fonctions `scatter` & `join` de MPI
    * Arguments : `-s XXX` avec XXX la taille des tableaux locaux (taille total = taille local* 4)





