//
// Created by pviolette on 13/09/18.
//

#include "repartition_algorithm.h"

void equalRepartition(std::vector<ThreadPosition>& result, const int vectorSize, const int threadCount){
    std::vector<int> eltByThreads(threadCount, vectorSize / threadCount);
    int left = vectorSize % threadCount;

    while(left > 0){
        for(unsigned long i = 0; i < eltByThreads.size() && left > 0; ++i){
            eltByThreads[i]++;
            left--;
        }
    }

    ThreadPosition first{0, eltByThreads[0], eltByThreads[0]};
    result[0] = first;

    for(unsigned long i = 1; i < threadCount; ++i){
        ThreadPosition pos{result[i - 1].end, result[i - 1].end + eltByThreads[i], eltByThreads[i]};
        result[i] = pos;
    }
}
