//
// Created by pviolette on 13/09/18.
//

#ifndef TP1_REPARTITION_ALGORITHM_H
#define TP1_REPARTITION_ALGORITHM_H

#include <vector>

struct ThreadPosition{
   int start;
    int end;
    int size;
};

/**
 * @param result
 * @param vectorSize
 * @param threadCount
 */
void equalRepartition(std::vector<ThreadPosition>& result, const int vectorSize, const int threadCount);

#endif //TP1_REPARTITION_ALGORITHM_H
