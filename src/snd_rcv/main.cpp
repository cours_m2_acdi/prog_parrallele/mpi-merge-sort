//
// Created by pviolette on 08/11/18.
//
#include <unistd.h>
#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <sstream>
#include <ctime>
#include <algorithm>

#include "../mmpi/mmpi.h"
#include "../merge.h"
#include "../utils/utils.h"
#include "../utils/ainsi_colors.h"
#include "../utils/repartition_algorithm.h"

#include "master.h"
#include "slave.h"


int main(int argc, char *argv[]) {
    MPI::Init(argc, argv);
    MMPI mmpi;

    if (mmpi.is_master()) {
        masterProcess(mmpi, argc, argv);
    } else {
        slaveProcess(mmpi);
    }

    mmpi.finalize();
    MPI::Finalize();

    exit(EXIT_SUCCESS);
}


