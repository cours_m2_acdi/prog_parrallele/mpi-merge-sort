//
// Created by etudiant on 09/11/18.
//

#include <algorithm>
#include "slave.h"


void slaveProcess(MMPI &mmpi) {
    int *local_data;

    mmpi.remote(0);

    int local_data_size;
    //Receive size of the array
    mmpi.recv(local_data_size);
    // each processor creates its local data
    local_data = new int[local_data_size];

    //Receive array
    mmpi.recv(local_data, local_data_size);

    //Sort
    //merge_sort(local_data, local_data_size);
    std::sort(local_data, local_data + local_data_size);
    //Signal process has finished sorting
    MPI::COMM_WORLD.Barrier();
    //Send result
    mmpi.remote(0);
    mmpi.send(local_data, local_data_size);
    delete[] local_data;

}