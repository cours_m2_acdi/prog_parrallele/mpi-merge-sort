//
// Created by etudiant on 09/11/18.
//

#ifndef MPI_SORT_SLAVE_H
#define MPI_SORT_SLAVE_H

#include "../mmpi/mmpi.h"

void slaveProcess(MMPI &mmpi);


#endif //MPI_SORT_SLAVE_H
