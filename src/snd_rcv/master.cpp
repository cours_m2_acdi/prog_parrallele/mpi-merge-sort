//
// Created by etudiant on 09/11/18.
//

#include <algorithm>
#include "master.h"
#include "../utils/utils.h"
#include "../utils/ainsi_colors.h"
#include "../merge.h"
#include "../cputimer/cpu_timer.h"

void masterProcess(MMPI &mmpi, int argc, char **argv) {
    //Generate array
    int opt;

    int global_data_size = 43;
    int *global_data = nullptr;

    while ((opt = getopt(argc, argv, "s:")) != -1) {
        switch (opt) {
            case 's':
                global_data_size = atoi(optarg);
                break;
            default: /* aq?aq */
                fprintf(stderr, "Usage: %s [-s size]",
                        argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    CPUTimer globalTimer;
    globalTimer.start();

    global_data_size = global_data_size;
    global_data = new int[global_data_size];

    populateArrayRandom(global_data, global_data_size);

    //Compute repartition of the array
    std::vector<ThreadPosition> positions(static_cast<unsigned long>(mmpi.max_cpus()));

    equalRepartition(positions, global_data_size, mmpi.max_cpus());

    CPUTimer cpuTimer;

    cpuTimer.start();
    scatterArray(mmpi, global_data, positions);


    //Sort first part of the array
    //merge_sort(global_data, positions[0].size);
    std::sort(global_data, global_data + positions[0].size);

    //Wait for all slaves to finish sorting
    MPI::COMM_WORLD.Barrier();
    //Receive other parts from the slaves
    joinArray(mmpi, global_data, positions);

    cpuTimer.stop();
    mmpi << "Sorting Parts time=" << cpuTimer.getTime() << MMPI::endl;

    cpuTimer.start();

    mergeArrayParts(global_data, global_data_size, positions);

    cpuTimer.stop();
    mmpi << "Merging time=" << cpuTimer.getTime() << MMPI::endl;


    globalTimer.stop();

    mmpi << "Total time=" << globalTimer.getTime() << MMPI::endl;
    //Check it's sorted
    if (isSorted(global_data, global_data_size)) {
        mmpi << ANSI_COLOR_GREEN << "Array size " << global_data_size << " is sorted" << ANSI_COLOR_RESET
             << MMPI::endl;
    } else {
        mmpi << ANSI_COLOR_RED << "Array is NOT sorted" << ANSI_COLOR_RESET << MMPI::endl;
    }

    delete[] global_data;
}

void scatterArray(MMPI &mmpi, int *array, std::vector<ThreadPosition> &positions) {
    // Send array parts
    for (int i = 1; i < mmpi.max_cpus(); ++i) {
        mmpi.remote(i);
        mmpi.send(positions[i].size); //Send size of the array so slaves can allocate memory
        mmpi.send(array + positions[i].start, positions[i].size); //Send array
    }
}

void joinArray(MMPI &mmpi, int *destArray, std::vector<ThreadPosition> &positions) {
    for (int i = 1; i < mmpi.max_cpus(); ++i) {
        mmpi.remote(i);
        mmpi.recv(destArray + positions[i].start, positions[i].size);
    }
}

void mergeArrayParts(int *array, int size, std::vector<ThreadPosition> &positions) {
    //Merge parts
    int mergedSize = positions[0].size; //Size of the sorted part of the array
    int *tmpDest = new int[size]; //Temp array to merge in
    for (int i = 1; i < positions.size(); ++i) {
        merge(array, mergedSize, array + positions[i].start, positions[i].size, tmpDest);
        mergedSize += positions[i].size;
        for (int j = 0; j < mergedSize; ++j) { //Copy merged into global_data
            array[j] = tmpDest[j];
        }
    }
    delete[] tmpDest;
}