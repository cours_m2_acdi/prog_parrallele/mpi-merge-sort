//
// Created by etudiant on 09/11/18.
//

#ifndef MPI_SORT_MASTER_H
#define MPI_SORT_MASTER_H

#include "../mmpi/mmpi.h"
#include <vector>
#include "../utils/repartition_algorithm.h"

void masterProcess(MMPI &mmpi, int argc, char **argv);

void scatterArray(MMPI &mmpi, int *array, std::vector<ThreadPosition> &positions);

void joinArray(MMPI &mmpi, int *destArray, std::vector<ThreadPosition> &positions);

void mergeArrayParts(int *array, int size, std::vector<ThreadPosition> &positions);


#endif //MPI_SORT_MASTER_H
