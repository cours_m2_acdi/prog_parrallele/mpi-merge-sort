//
// Created by etudiant on 08/11/18.
//

#include <algorithm>
#include <iostream>
#include "merge.h"

void merge(int * firstArray, int firstSize, int * secondArray, int secondSize, int * dest){
    int i = 0;
    int j = 0;
    int k = 0;

    while(i < firstSize and j < secondSize){
        int min;
        if(firstArray[i] < secondArray[j]){
            min = firstArray[i];
            ++i;
        }else{
            min = secondArray[j];
            ++j;
        }
        dest[k] = min;
        ++k;
    }

    for(; i < firstSize; ++i){
        dest[k] = firstArray[i];
        ++k;
    }
    for(; j < secondSize; ++j){
        dest[k] = secondArray[j];
        ++k;
    }
}

void split(int * array, int first, int last){
    if(last - first < 2){
        if(last - first > 0){
            if(array[first] > array[first + 1]){
                int tmp = array[first];
                array[first] = array[first + 1];
                array[first + 1] = tmp;
            }
        }
    } else {
        int splitIndex = (first + last) / 2;
        split(array, first, splitIndex);
        split(array, splitIndex + 1, last);

        int * dest = new int[last - first + 1];

        int sizeFirst = splitIndex - first + 1;
        int sizeSecond = last - splitIndex;
        merge(array + first, sizeFirst, array + splitIndex + 1, sizeSecond, dest);

        for(int i = first; i <= last; ++i){
            array[i] = dest[i - first];
        }

        delete[] dest;
    }
}

void merge_sort(int * array, int size){
    split(array, 0, size - 1);
}
