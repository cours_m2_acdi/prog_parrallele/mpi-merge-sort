#include <unistd.h>
#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <sstream>
#include <ctime>
#include <algorithm>

#include "../mmpi/mmpi.h"
#include "../merge.h"
#include "../utils/utils.h"

#include "../utils/ainsi_colors.h"

int main(int argc, char * argv[]) {

    int local_data_size = 10;
    int opt;
    bool verbose = false;
    std::srand(static_cast<unsigned int>(std::time(NULL)));
    while ((opt = getopt(argc, argv, "s:v")) != -1) {
        switch (opt) {
            case 's':
                local_data_size = atoi(optarg);
                break;
            case 'v':
                verbose = true;
                break;
            default: /* aq?aq */
                fprintf(stderr, "Usage: %s [-s size]",
                        argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    MPI::Init(argc, argv);
    MMPI mmpi;

    int *local_data;
    int global_data_size = 0;
    int *global_data = nullptr;

    std::srand(static_cast<unsigned int>(std::time(nullptr) * mmpi.cpu_rank()));

    if (mmpi.is_master()) {
        // master process will gather data from others
        global_data_size = mmpi.max_cpus() * local_data_size;
        global_data = new int[global_data_size];
        for (int i = 0; i < global_data_size; ++i) global_data[i] =  std::rand() % 10000;
    }

    // each processor creates its local data
    local_data = new int[local_data_size];

    mmpi.scatter(global_data, local_data_size, local_data);

    if (verbose) {
        logArray(local_data, local_data_size, mmpi, "local array=", "");
    }

    merge_sort(local_data, local_data_size);

    mmpi.gather(local_data, local_data_size, global_data);

    if (mmpi.is_master()) {
        if (verbose) {
            logArray(global_data, global_data_size, mmpi, "Post-gather global array = ", "");
        }

        //Now merge arrays
        int *dest = new int[global_data_size];
        for(int i = 1; i < mmpi.max_cpus(); ++i){
            merge(global_data, i * local_data_size, global_data + (i * local_data_size), local_data_size, dest);

            for(int j = 0; j < (i + 1) * local_data_size; ++j){
                global_data[j] = dest[j];
            }
        }
        delete[] dest;

        if (verbose) {
            logArray(global_data, global_data_size, mmpi, "\033[31;1;4mSorted global array = ", "\033[0m");
        }

        if(isSorted(global_data, global_data_size)){
            mmpi << ANSI_COLOR_GREEN << "Array size " << global_data_size <<" is sorted" << ANSI_COLOR_RESET << MMPI::endl;
        }else{
            mmpi << ANSI_COLOR_RED << "Array is NOT sorted" << ANSI_COLOR_RESET << MMPI::endl;
        }

        delete[] global_data;
    }

    delete[] local_data;

    mmpi.finalize();
    MPI::Finalize();

    exit(EXIT_SUCCESS);
}